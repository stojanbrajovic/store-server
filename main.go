package main

import (
	"log"
	"net/http"
	"store-server/pkg/item"
	"store-server/pkg/offer"
	"store-server/pkg/parameter"
	"store-server/pkg/paramvalue"
	"store-server/pkg/user"
)

func main() {
	user.StartHTTPHandlers()
	item.StartHTTPHandlers()
	offer.StartHTTPHandlers()
	parameter.StartHTTPHandlers()
	paramvalue.StartHTTPHandlers()
	log.Fatal(http.ListenAndServe(":8081", nil))
}
