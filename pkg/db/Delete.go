package db

import (
	"github.com/boltdb/bolt"
)

// Delete - delete item with id from bucket
func Delete(bucketName string, id string) error {
	db, err := open()
	if err != nil {
		return err
	}
	defer db.Close()

	return db.Update(func(tx *bolt.Tx) error {
		bucket := tx.Bucket([]byte(bucketName))
		return bucket.Delete([]byte(id))
	})
}
