package db

import (
	"encoding/json"

	"github.com/boltdb/bolt"
)

// Iterator - function to call after the value has been retrieved from bucket
// returns true when the iterating should stop
type Iterator = func(id string) bool

// Iterate - iterate through all objects in a bucket
func Iterate(bucketName string, iteratorObject interface{}, iterator Iterator) error {
	db, err := open()
	if err != nil {
		return err
	}
	defer db.Close()

	return db.View(func(tx *bolt.Tx) error {
		// Assume bucket exists and has keys
		bucket := tx.Bucket([]byte(bucketName))

		cursor := bucket.Cursor()

		for key, value := cursor.First(); key != nil; key, value = cursor.Next() {
			err = json.Unmarshal(value, iteratorObject)
			if err != nil {
				return err
			}
			shouldStop := iterator(string(key))
			if shouldStop {
				return nil
			}
		}

		return nil
	})
}
