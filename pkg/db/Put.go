package db

import (
	"encoding/json"

	"github.com/boltdb/bolt"
)

// Create - creates new item in database, returns new id
func Create(item interface{}, bucketName string) (string, error) {
	return put("", item, bucketName)
}

// Put - updates item in database
func Put(id string, item interface{}, bucketName string) error {
	_, err := put(id, item, bucketName)
	return err
}

func put(id string, item interface{}, bucketName string) (string, error) {
	db, err := open()
	if err != nil {
		return "", err
	}
	defer db.Close()

	err = db.Update(func(tx *bolt.Tx) error {
		bucket, err := tx.CreateBucketIfNotExists([]byte(bucketName))
		if err != nil {
			return err
		}

		if id == "" {
			id, err = createNewID(bucket)
			if err != nil {
				return err
			}
		}
		buffer, err := json.Marshal(item)
		bucket.Put([]byte(id), buffer)

		return nil
	})

	return id, err
}
