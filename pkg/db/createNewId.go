package db

import (
	"fmt"

	"github.com/boltdb/bolt"
)

func createNewID(bucket *bolt.Bucket) (string, error) {
	id, err := bucket.NextSequence()
	if err != nil {
		return "", err
	}
	idString := fmt.Sprint(id)
	return idString, nil
}
