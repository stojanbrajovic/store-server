package db

import (
	"encoding/json"
	"errors"
	"fmt"

	"github.com/boltdb/bolt"
)

// Get - fetch an item with id key from bucket, and stores it in store object
func Get(bucketName string, id string, storeObject interface{}) error {
	db, err := open()
	if err != nil {
		return err
	}
	defer db.Close()

	return db.View(func(tx *bolt.Tx) error {
		bucket := tx.Bucket([]byte(bucketName))
		byteResult := bucket.Get([]byte(id))
		if byteResult == nil {
			errorText := fmt.Sprintf("No object found in %s with %s id", bucketName, id)
			return errors.New(errorText)
		}
		return json.Unmarshal(byteResult, storeObject)
	})
}
