package db

import (
	"github.com/boltdb/bolt"
)

const dbFileName = "store.db"
const readWriteFileMode = 0600

func open() (*bolt.DB, error) {
	return bolt.Open(dbFileName, readWriteFileMode, nil)
}
