package httpresponse

import (
	"encoding/json"
	"errors"
	"net/http"
)

// Error - write error
func Error(w http.ResponseWriter, err error) {
	http.Error(w, err.Error(), http.StatusInternalServerError)
}

// InvalidRequestMethod - write invalid request method error
func InvalidRequestMethod(w http.ResponseWriter) {
	Error(w, errors.New("Invalid request method"))
}

// JSON - write json response
func JSON(w http.ResponseWriter, data interface{}) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	err := json.NewEncoder(w).Encode(data)
	if err != nil {
		Error(w, err)
	}
}
