package item

import (
	"store-server/pkg/db"
)

const bucket = "item"

func get(id string) (*Item, error) {
	item := &Item{}
	err := db.Get(bucket, id, item)
	return item, err
}

func create(item *Item) (string, error) {
	return db.Create(item, bucket)
}

func delete(id string) error {
	return db.Delete(bucket, id)
}

func update(item *Item, id string) error {
	return db.Put(id, item, bucket)
}

type iterator = func(*Item, string) bool

func iterate(iterate iterator) error {
	item := &Item{}
	iterator := func(id string) bool {
		return iterate(item, id)
	}
	return db.Iterate(bucket, item, iterator)
}
