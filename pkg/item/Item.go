package item

import "store-server/pkg/store"

type parameterValue struct {
	ParameterID string  `json:"parameterId"`
	Value       float64 `json:"value"`
}

// Item - single store item - it can contain multiple sub items
type Item struct {
	store.NamedObject
	ChildrenIDs       []string         `json:"childrenIds,omitempty"`
	ParameterIDs      []string         `json:"parameterIds,omitempty"`
	ParameterValueIDs []string         `json:"parameterValueIds,omitempty"`
	ParameterValues   []parameterValue `json:"parameterValues,omitempty"`
	CreatorID         string           `json:"creatorId"`
}
