package item

import (
	"net/http"
	"store-server/pkg/httpresponse"
)

func itemHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		postItem(w, r)
	} else if r.Method == "GET" {
		getItem(w, r)
		//} else if r.Method == "PATCH" {
		// 	updateUser(w, r)
	} else {
		httpresponse.InvalidRequestMethod(w)
	}
}

func postParamToItem(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		postNewParameter(w, r)
	} else {
		httpresponse.InvalidRequestMethod(w)
	}
}

// StartHTTPHandlers - init all http item handlers
func StartHTTPHandlers() {
	http.HandleFunc("/item", itemHandler)
	http.HandleFunc("/paramToItem", itemHandler)
}
