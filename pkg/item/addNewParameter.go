package item

import (
	"encoding/json"
	"net/http"
	"store-server/pkg/httpresponse"
	"store-server/pkg/parameter"
)

type postNewParamReq struct {
	Param  parameter.Parameter `json:"parameter"`
	ItemID string              `json:"itemId"`
}

func postNewParameter(w http.ResponseWriter, r *http.Request) {
	req := &postNewParamReq{}
	err := json.NewDecoder(r.Body).Decode(req)
	if err != nil {
		httpresponse.Error(w, err)
		return
	}

	paramID, err := addNewParameter(&req.Param, req.ItemID)
	if err != nil {
		httpresponse.Error(w, err)
		return
	}

	httpresponse.JSON(w, paramID)
}

func addNewParameter(param *parameter.Parameter, itemID string) (string, error) {
	item, err := get(itemID)
	if err != nil {
		return "", err
	}

	paramID, err := parameter.Create(param)
	if err != nil {
		return "", err
	}

	paramIDs := append(item.ParameterIDs, paramID)
	item.ParameterIDs = paramIDs
	err = update(item, itemID)

	return paramID, err
}
