package item

import (
	"net/http"
	"store-server/pkg/httpresponse"
	"store-server/pkg/parameter"
)

type responseGetItem struct {
	Item       Item                  `json:"item"`
	Parameters []parameter.Parameter `json:"parameters"`
}

func getItem(w http.ResponseWriter, r *http.Request) {
	id := r.URL.Query().Get("id")
	item, err := get(id)
	if err != nil {
		httpresponse.Error(w, err)
		return
	}

	parameters, err := parameter.Get(item.ParameterIDs)

	if err != nil {
		httpresponse.Error(w, err)
		return
	}

	response := responseGetItem{*item, parameters}
	httpresponse.JSON(w, response)
}
