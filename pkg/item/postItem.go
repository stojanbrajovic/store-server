package item

import (
	"encoding/json"
	"net/http"
	"store-server/pkg/httpresponse"
	"store-server/pkg/user"
)

func postItem(w http.ResponseWriter, r *http.Request) {
	err := user.CheckAdmin(r)
	if err != nil {
		httpresponse.Error(w, err)
		return
	}

	item := &Item{}
	err = json.NewDecoder(r.Body).Decode(item)

	if err != nil {
		httpresponse.Error(w, err)
		return
	}

	id, err := create(item)

	if err != nil {
		httpresponse.Error(w, err)
		return
	}

	httpresponse.JSON(w, id)
}
