package offer

import "store-server/pkg/store"

// Offer - price offer for a single item in store.
type Offer struct {
	store.NamedObject
	Price       int    `json:"price"`
	ItemID      string `json:"itemId"`
	UserID      string `json:"userId,omitempty"`
	Quantity    int    `json:"quantity,omitempty"`
	ContactInfo string `json:"contactInfo,omitempty"`
}
