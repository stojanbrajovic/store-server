package offer

import (
	"encoding/json"
	"errors"
	"net/http"
	"store-server/pkg/httpresponse"
)

func postOffer(w http.ResponseWriter, r *http.Request) {
	offer := &Offer{}
	err := json.NewDecoder(r.Body).Decode(offer)
	if err != nil {
		httpresponse.Error(w, err)
		return
	}

	if offer.ItemID == "" {
		httpresponse.Error(w, errors.New("Must provide itemId"))
		return
	}

	id, err := Create(offer)
	if err != nil {
		httpresponse.Error(w, err)
		return
	}

	httpresponse.JSON(w, id)
}

func getOffer(w http.ResponseWriter, r *http.Request) {
	id := r.URL.Query().Get("id")
	offer, err := Get(id)
	if err != nil {
		httpresponse.Error(w, err)
		return
	}

	httpresponse.JSON(w, offer)
}

func offerRouteHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		postOffer(w, r)
	} else if r.Method == "GET" {
		getOffer(w, r)
	} else {
		httpresponse.InvalidRequestMethod(w)
	}
}

// StartHTTPHandlers - initialize all route handlers for offer
func StartHTTPHandlers() {
	http.HandleFunc("/offer", offerRouteHandler)
}
