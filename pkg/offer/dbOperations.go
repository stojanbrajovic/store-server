package offer

import (
	"store-server/pkg/db"
)

const bucket = "offer"

// Get - get single item offer from db
func Get(id string) (*Offer, error) {
	offer := &Offer{}
	err := db.Get(bucket, id, offer)
	return offer, err
}

// Create - create new item offer in db
func Create(offer *Offer) (string, error) {
	return db.Create(offer, bucket)
}

// Delete - delete item offer from db
func Delete(id string) error {
	return db.Delete(bucket, id)
}

// Update - update existing item offer in db
func Update(offer *Offer, id string) error {
	return db.Put(id, offer, bucket)
}

// Iterator - iterates through single item offer in db; return true to stop iteration
type Iterator = func(*Offer, string) bool

// Iterate - iterate through all item offers in db
func Iterate(iterator Iterator) error {
	offer := &Offer{}
	i := func(id string) bool {
		return iterator(offer, id)
	}
	return db.Iterate(bucket, offer, i)
}
