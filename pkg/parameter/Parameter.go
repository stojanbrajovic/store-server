package parameter

import "store-server/pkg/store"

// Parameter - parameter for an item. Determines item properties
type Parameter struct {
	store.NamedObject
	Min                  float64  `json:"min,omitempty"`
	Max                  float64  `json:"max,omitempty"`
	SelectableByRange    bool     `json:"selectableByRange,omitempty"`
	Step                 float64  `json:"step,omitempty"`
	PossibleValueIDs     []string `json:"sossibleValueIds,omitempty"`
	CustomValuesDisabled bool     `json:"customValuesDisabled,omitempty"`
}
