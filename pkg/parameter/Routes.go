package parameter

import (
	"encoding/json"
	"net/http"
	"store-server/pkg/httpresponse"
	"strings"
)

func postParam(w http.ResponseWriter, r *http.Request) {
	param := &Parameter{}
	err := json.NewDecoder(r.Body).Decode(param)
	if err != nil {
		httpresponse.Error(w, err)
		return
	}

	id, err := Create(param)
	if err != nil {
		httpresponse.Error(w, err)
		return
	}

	httpresponse.JSON(w, id)
}

func getParams(w http.ResponseWriter, r *http.Request) {
	idsString := r.URL.Query().Get("ids")
	ids := strings.Split(idsString, ",")
	params, err := Get(ids)
	if err != nil {
		httpresponse.Error(w, err)
		return
	}

	httpresponse.JSON(w, params)
}

func updateParam(w http.ResponseWriter, r *http.Request) {
	param := &Parameter{}
	err := json.NewDecoder(r.Body).Decode(param)
	if err != nil {
		httpresponse.Error(w, err)
		return
	}

	httpresponse.JSON(w, true)
}

func routeHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		postParam(w, r)
	} else if r.Method == "GET" {
		getParams(w, r)
	} else if r.Method == "PATCH" {
		updateParam(w, r)
	} else {
		httpresponse.InvalidRequestMethod(w)
	}
}

// StartHTTPHandlers - initialize all route handlers for parameter
func StartHTTPHandlers() {
	http.HandleFunc("/parameter", routeHandler)
}
