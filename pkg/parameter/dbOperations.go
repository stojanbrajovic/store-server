package parameter

import (
	"store-server/pkg/db"
	"store-server/pkg/store"
)

const parameterBucket = "parameter"

// Create - create new parameter in db
func Create(param *Parameter) (string, error) {
	return db.Create(param, parameterBucket)
}

// Delete - delete parameter
func Delete(id string) error {
	return db.Delete(parameterBucket, id)
}

// Update - update existing parameter
func Update(param *Parameter, id string) error {
	return db.Put(id, param, parameterBucket)
}

// Iterator - iterate through single parameter; return true to stop iteration
type Iterator = func(param *Parameter, id string) bool

// Iterate - iterate through all parameters
func Iterate(iterator Iterator) error {
	param := &Parameter{}
	i := func(id string) bool {
		return iterator(param, id)
	}
	return db.Iterate(parameterBucket, param, i)
}

// Get - get parameters with provided ids
func Get(ids []string) ([]Parameter, error) {
	numberOfParameters := len(ids)
	parameters := make([]Parameter, numberOfParameters)

	index := 0
	err := Iterate(func(param *Parameter, id string) bool {
		if store.IsIDInArray(id, ids) {
			parameters[index] = Parameter(*param)
			index++
		}

		return index == numberOfParameters
	})

	return parameters, err
}
