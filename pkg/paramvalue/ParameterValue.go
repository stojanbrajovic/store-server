package paramvalue

import (
	"store-server/pkg/store"
)

// ParameterValue - value of a parameter
type ParameterValue struct {
	store.NamedObject
	Value        float64  `json:"value,omitempty"`
	ParameterIDs []string `json:"parameterId,omitempty"`
}
