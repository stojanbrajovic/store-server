package paramvalue

import (
	"store-server/pkg/db"
	"store-server/pkg/store"
)

const bucket = "parameterValue"

// Get - get parameters with provided ids
func Get(ids []string) ([]ParameterValue, error) {
	numberOfValues := len(ids)
	values := make([]ParameterValue, numberOfValues)

	index := 0
	err := Iterate(func(param *ParameterValue, id string) bool {
		if store.IsIDInArray(id, ids) {
			values[index] = ParameterValue(*param)
			index++
		}

		return index == numberOfValues
	})

	return values, err
}

// Create - create new item offer in db
func Create(value *ParameterValue) (string, error) {
	return db.Create(value, bucket)
}

// Delete - delete item offer from db
func Delete(id string) error {
	return db.Delete(bucket, id)
}

// Update - update existing item offer in db
func Update(value *ParameterValue, id string) error {
	return db.Put(id, value, bucket)
}

// Iterator - iterates through single item offer in db; return true to stop iteration
type Iterator = func(*ParameterValue, string) bool

// Iterate - iterate through all item offers in db
func Iterate(iterator Iterator) error {
	value := &ParameterValue{}
	i := func(id string) bool {
		return iterator(value, id)
	}
	return db.Iterate(bucket, value, i)
}
