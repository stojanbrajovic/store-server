package paramvalue

import (
	"encoding/json"
	"net/http"
	"store-server/pkg/httpresponse"
	"strings"
)

func postParameterValue(w http.ResponseWriter, r *http.Request) {
	value := &ParameterValue{}
	err := json.NewDecoder(r.Body).Decode(value)
	if err != nil {
		httpresponse.Error(w, err)
		return
	}

	id, err := Create(value)
	if err != nil {
		httpresponse.Error(w, err)
		return
	}

	httpresponse.JSON(w, id)
}

func getParameterValues(w http.ResponseWriter, r *http.Request) {
	idsString := r.URL.Query().Get("ids")
	ids := strings.Split(idsString, ",")
	params, err := Get(ids)
	if err != nil {
		httpresponse.Error(w, err)
		return
	}

	httpresponse.JSON(w, params)
}

func updateParameterValue(w http.ResponseWriter, r *http.Request) {
	value := &ParameterValue{}
	err := json.NewDecoder(r.Body).Decode(value)
	if err != nil {
		httpresponse.Error(w, err)
		return
	}

	httpresponse.JSON(w, true)
}

func routeHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		postParameterValue(w, r)
	} else if r.Method == "GET" {
		getParameterValues(w, r)
	} else if r.Method == "PATCH" {
		updateParameterValue(w, r)
	} else {
		httpresponse.InvalidRequestMethod(w)
	}
}

// StartHTTPHandlers - initialize all route handlers for parameter
func StartHTTPHandlers() {
	http.HandleFunc("/paremeterValue", routeHandler)
}
