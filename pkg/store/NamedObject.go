package store

// NamedObject - object with a name and description in the store
type NamedObject struct {
	Name        string `json:"name,omitempty"`
	Description string `json:"description,omitempty"`
}
