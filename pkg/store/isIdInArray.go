package store

// IsIDInArray - checks if an id is in array of ids
func IsIDInArray(id string, idArray []string) bool {
	for _, currID := range idArray {
		if currID == id {
			return true
		}
	}

	return false
}
