package token

import (
	"store-server/pkg/db"
	"time"
)

const secondsInADay = 24 * 60 * 60
const longDuration = 100 * secondsInADay
const shortDuration = secondsInADay

// Create - create new token and put it in database
func Create(userID string, isLong bool) (string, error) {
	tokenValue, err := generate(userID)
	if err != nil {
		return "", err
	}

	var duration int64 = shortDuration
	if isLong {
		duration = longDuration
	}
	token := token{tokenValue, userID, time.Now().Unix(), duration}
	// Check if it already exists ...
	err = db.Put(tokenValue, token, tokenBucket)

	return tokenValue, err
}
