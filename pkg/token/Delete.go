package token

import (
	"store-server/pkg/db"
)

// Delete - delete token
func Delete(tkn string) error {
	return db.Delete(tokenBucket, tkn)
}
