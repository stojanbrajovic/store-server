package token

import "net/http"

// GetFromHTTPReq - return token from http request
func GetFromHTTPReq(r *http.Request) string {
	return r.Header.Get("Authorization")
}
