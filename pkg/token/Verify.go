package token

import (
	"store-server/pkg/db"
	"time"
)

const tokenDurationDays = 100
const tokenDurationSeconds = tokenDurationDays * 24 * 60 * 60

// Verify - verify provided token string
func Verify(tokenValue string) (string, error) {
	token := token{}
	err := db.Get(tokenBucket, tokenValue, token)
	if err != nil {
		return "", err
	}

	if time.Now().Unix()-token.CreatedAt > token.Duration {
		return "", ErrTokenExpired
	}

	return token.UserID, nil
}
