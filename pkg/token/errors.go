package token

import "errors"

// ErrTokenExpired - error for expired tokens
var ErrTokenExpired = errors.New("Token expired")

// ErrTokenInvalid - error for verifying invalid token
var ErrTokenInvalid = errors.New("Token invalid")
