package token

import (
	"crypto/md5"

	"golang.org/x/crypto/bcrypt"
)

func generate(inputString string) (string, error) {
	hash, err := bcrypt.GenerateFromPassword([]byte(inputString), bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}

	hasher := md5.New()
	_, err = hasher.Write(hash)
	if err != nil {
		return "", err
	}

	return string(hasher.Sum(nil)), nil
}
