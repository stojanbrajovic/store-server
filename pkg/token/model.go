package token

type token struct {
	ID        string `json:"id"`
	UserID    string `json:"userId"`
	CreatedAt int64  `json:"createdAt"`
	Duration  int64  `json:"duration"`
}
