package user

import (
	"net/http"
	"store-server/pkg/db"
	"store-server/pkg/token"
)

const adminsBucket = "admins"

func addAdmin(userID string) error {
	return db.Put(userID, userID, adminsBucket)
}

// CheckAdmin - verify that the sent token belongs to an admin
func CheckAdmin(r *http.Request) error {
	tkn := token.GetFromHTTPReq(r)
	userID, err := token.Verify(tkn)
	if err != nil {
		return err
	}
	id := ""
	return db.Get(adminsBucket, userID, &id)
}
