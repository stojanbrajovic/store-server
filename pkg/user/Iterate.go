package user

import (
	"store-server/pkg/db"
)

// Iterator - returns true to stop iterating
type Iterator = func(user *User) bool

// Iterate - iterate through all users
func Iterate(iterator Iterator) error {
	user := &User{}
	innerIterator := func(key string) bool {
		user.ID = key
		return iterator(user)
	}
	err := db.Iterate(userBucket, user, innerIterator)
	return err
}
