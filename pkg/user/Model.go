package user

// User - registered store user
type User struct {
	Username     string `json:"username"`
	Email        string `json:"email,omitempty"`
	Name         string `json:"name,omitempty"`
	LastName     string `json:"lastName,omitempty"`
	ID           string `json:"id,omitempty"`
	PasswordHash []byte `json:"passwordHash,omitempty"`
}
