package user

import (
	"errors"
	"store-server/pkg/db"
)

const userBucket = "User"

const minPasswordLength = 4

func create(user User, pwd string) (string, error) {
	if len(pwd) < minPasswordLength {
		return "", errors.New("Password too short")
	}
	hash, err := hashPass(pwd)
	if err != nil {
		return "", err
	}
	user.PasswordHash = hash
	return db.Create(user, userBucket)
}

func get(id string) (User, error) {
	user := &User{}
	err := db.Get(userBucket, id, user)
	return *user, err
}

func delete(id string) error {
	return db.Delete(userBucket, id)
}

func update(updatedUser *User) error {
	id := updatedUser.ID
	existingUser, err := get(id)
	if err != nil {
		return err
	}
	updatedUser.PasswordHash = existingUser.PasswordHash
	return db.Put(updatedUser.ID, updatedUser, userBucket)
}

func changePassword(id string, oldPassword string, newPassword string) error {
	if len(newPassword) < minPasswordLength {
		return errors.New("Password too short")
	}

	user, err := get(id)
	if err != nil {
		return err
	}

	err = checkPass(oldPassword, user.PasswordHash)
	if err != nil {
		return err
	}

	hash, err := hashPass(newPassword)
	if err != nil {
		return err
	}

	user.PasswordHash = hash
	return db.Put(id, user, userBucket)
}
