package user

import (
	"encoding/json"
	"errors"
	"net/http"
	"store-server/pkg/db"
	"store-server/pkg/httpresponse"
	"store-server/pkg/token"
)

type signUpRequest struct {
	User     User   `json:"user"`
	Password string `json:"password"`
}

func decode(r *http.Request, requestObject interface{}) error {
	return json.NewDecoder(r.Body).Decode(requestObject)
}

func signUp(w http.ResponseWriter, r *http.Request) {
	request := &signUpRequest{}
	err := decode(r, request)
	if err != nil {
		httpresponse.Error(w, err)
		return
	}

	id, err := create(request.User, request.Password)
	if err != nil {
		httpresponse.Error(w, err)
		return
	}

	httpresponse.JSON(w, id)
}

func getUser(w http.ResponseWriter, r *http.Request) {
	id := r.URL.Query().Get("id")
	user, err := get(id)
	if err != nil {
		httpresponse.Error(w, err)
		return
	}

	user.PasswordHash = []byte{}

	httpresponse.JSON(w, user)
}

func updateUser(w http.ResponseWriter, r *http.Request) {
	tkn := token.GetFromHTTPReq(r)

	id, err := token.Verify(tkn)
	if err != nil {
		httpresponse.Error(w, err)
		return
	}

	u := &User{}
	err = decode(r, u)
	if err != nil {
		httpresponse.Error(w, err)
		return
	}

	if id != u.ID {
		err = errors.New("Can't change another user")
		httpresponse.Error(w, err)
		return
	}

	err = update(u)
	if err != nil {
		httpresponse.Error(w, err)
		return
	}
}

func deleteUser(w http.ResponseWriter, r *http.Request) {
	err := db.Delete(userBucket, r.URL.Query().Get("id"))
	if err != nil {
		httpresponse.Error(w, err)
		return
	}

	httpresponse.JSON(w, "Success")
}

func userHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		signUp(w, r)
	} else if r.Method == "GET" {
		getUser(w, r)
	} else if r.Method == "PATCH" {
		updateUser(w, r)
		// } else if r.Method == "DELETE" {
		// 	deleteUser(w, r)
	} else {
		httpresponse.InvalidRequestMethod(w)
	}
}

type loginRequest struct {
	UserName string `json:"userName"`
	Password string `json:"password"`
	Remember bool   `json:"remember,omitempty"`
}

func loginHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		httpresponse.InvalidRequestMethod(w)
		return
	}

	request := &loginRequest{}
	err := decode(r, request)
	if err != nil {
		httpresponse.Error(w, err)
		return
	}

	t, err := login(request.UserName, request.Password, request.Remember)
	if err != nil {
		httpresponse.Error(w, err)
		return
	}

	httpresponse.JSON(w, t)
}

func logout(w http.ResponseWriter, r *http.Request) {
	t := token.GetFromHTTPReq(r)
	err := token.Delete(t)
	if err != nil {
		httpresponse.Error(w, err)
		return
	}

	httpresponse.JSON(w, "success")
}

// StartHTTPHandlers - start all user related http handlers
func StartHTTPHandlers() {
	http.HandleFunc("/user", userHandler)
	http.HandleFunc("/login", loginHandler)
	http.HandleFunc("/logout", logout)
}
