package user

import (
	"store-server/pkg/token"
)

func findUsernameOrEmail(usernameEmail string) (User, error) {
	var result User
	iterator := func(user *User) bool {
		if user.Username == usernameEmail || user.Email == usernameEmail {
			result = User(*user)
			return true
		}
		return false
	}

	err := Iterate(iterator)

	return result, err
}

func login(usernameEmail, pwd string, rememberUser bool) (string, error) {
	user, err := findUsernameOrEmail(usernameEmail)
	if err != nil {
		return "", err
	}

	err = checkPass(pwd, user.PasswordHash)
	if err != nil {
		return "", err
	}

	return token.Create(user.ID, rememberUser)
}
