package user

import (
	"golang.org/x/crypto/bcrypt"
)

func hashPass(pwd string) ([]byte, error) {
	// Use GenerateFromPassword to hash & salt pwd.
	// MinCost is just an integer constant provided by the bcrypt
	// package along with DefaultCost & MaxCost.
	// The cost can be any value you want provided it isn't lower
	// than the MinCost (4)
	return bcrypt.GenerateFromPassword([]byte(pwd), bcrypt.DefaultCost)
}

func checkPass(pwd string, hash []byte) error {
	return bcrypt.CompareHashAndPassword(hash, []byte(pwd))
}
